<?php
 
 namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\User;
use App\Customers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class CustomersController extends Controller
 {
     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index()
     {
        //
        $id = Auth::id();
        $customers = Customers::all();
        return view('customers.index', ['customers' => $customers, 'id' =>$id]);
     }
 
     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
        //
        return view('customers.create');
     }
 
     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {
        //
        $customer = new Customers();
          $id=Auth::id();
          $boss = DB::table('users')->where('id',$id)->first();
           $username = $boss->name;
           $customer->name = $request->name;
           $customer->email = $request->email;
           $customer->phone = $request->phone;
           $customer->user_id = $id;
           $customer->status = 0;
           $customer->username = $username;
           $customer->save();
           return redirect('customers');
     }
 
     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         //
     }
 
     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
        $customer = Customers::find($id);
        $id= Auth::id();
        $user_id =  $customer ->user_id;
        if (Gate::denies('manager')) {
            if ($user_id != $id) 
            {
            abort(403,"Sorry you do not hold permission to edit this Customer..");
            } 
        }
        
        return view('customers.edit', ['customer' => $customer]);
     }
 
     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
        //
        $customer = Customers::find($id);
        $customer->update($request -> all());
        return redirect('customers');
     }
 
     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
        if (Gate::denies('manager')) {
                        abort(403,"Sorry you are not allowed to delete Customers..");
                    } 
        $customer = Customers::findOrFail($id);
        $customer->delete();
        return redirect('customers'); 
     }

     public function change_status($id,$status,Request $request)
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to change customers status..");
        } 
        $customer = Customers::find($id);
        $customer->status = 1; 
        $customer->update($request -> all());
        return redirect('customers');
    }
 }