<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
       // $this->registerPolicies();

      Gate::define('manager', function ($user) {
           return $user->role == 'manager';
        });
        Gate::define('salesrep', function ($user) {
            return $user->role == 'salesrep';
         });
 

    }
}
