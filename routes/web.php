<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('customers/status/{id}/{status}', 'CustomersController@change_status')->name('customers.change_status');
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('customers','CustomersController')->middleware('auth');
Route::get('customers/delete/{id}', 'CustomersController@destroy')->name('delete');