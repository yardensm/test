@extends('layouts.app')
@section('content')

 <h1>Create a Customer</h1>
 
<form method = 'post' action = "{{action('CustomersController@store')}}">
@csrf 
 <div class = "form-group">
 <label for = "name" > Customer Name </label>
 <input type = "text" class = "form-control" name = "name">
 </div>

<div class = "form-group">
 <label for = "email" > Customer Email </label>
 <input type = "text" class = "form-control" name = "email">
 </div>

 <div class = "form-group">
 <label for = "phone" > Customer Phone Number </label>
 <input type = "text" class = "form-control" name = "phone">
 </div>

 
 <div class = "form-group">
 <input type = "submit" class = "form-control" name = "submit" value = "Save">
 </div> 
  
</form>
@endsection