@extends('layouts.app')
@section('content')
<h1>This is your Customers list</h1>
 
<h2><a href ="{{route('customers.create')}}"> Create a new Customer </a></h2>
 <table class = 'table' border="4">
 <thead>  
      <tr>  
      <th>Customer Name</th>
      <th>Sales Representative</th> 
      <th>Email</th> 
      <th>Phone</th>
      <th>Edit Customer Info</th>
      <th>Delete Customer</th>
      @cannot('salesrep') <th>Deal Status</th>@endcannot
      </tr>  
  </thead>
     
     @foreach($customers as $customer)
         <tr>
         @if ($customer->status)
              <tr style="background-color: #228B22">
         @else 
               <tr>
        @endif
                        @if ($customer->user_id == $id)
                                @php $bold = 'font-weight:bold' @endphp
                        @else 
                                @php $bold = '' @endphp
                        @endif
            <td style = "{{ $bold }}" >{{$customer->name }}</td>
            <td style = "{{ $bold }}" >{{$customer->username }}</td>
            <td style = "{{ $bold }}" >{{$customer->email }}</td>
            <td style = "{{ $bold }}" >{{$customer->phone }}</td>
            <td style = "{{ $bold }}" ><a href ="{{route('customers.edit', $customer ->id)}}" >Edit</a></td>
            <td style = "{{ $bold }}" > @cannot('salesrep')<a href="{{route('delete', $customer->id)}}">@endcannot Delete</a></td>
             
            @cannot('salesrep')
            @if ($customer->status)        
           @else
                 <td>  <a href="{{route('customers.change_status', [ $customer->id, $customer->status ])}}">Deal closed</a> </td>
          @endif
          @endcannot
         </tr>
             @endforeach
</table>


@endsection